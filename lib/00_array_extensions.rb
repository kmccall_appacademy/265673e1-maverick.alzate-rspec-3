# Sum
#
# Write an Array method, `sum`, that returns the sum of the elements in the
# array. You may assume that all of the elements are integers.

class Array
  def sum
    return 0 if self.empty?
    self.inject(:+)
  end
end

# Square
#
# Write an array method, `square`, that returns a new array containing the
# squares of each element. You should also implement a "bang!" version of this
# method, which mutates the original array.

class Array
  def square!
    return self if self.empty?

    count = 0
    while count < self.length
      self[count] = self[count]**2
      count += 1
    end

    self
  end

  def square
    return self if self.empty?

    copy = self.dup
    copy.square!
  end
end

# Finding Uniques
#
# Monkey-patch the Array class with your own `uniq` method, called
# `my_uniq`. The method should return the unique elements, in the order
# they first appeared:
#
# ```ruby
# [1, 2, 1, 3, 3].my_uniq # => [1, 2, 3]
# ```
#
# Do not use the built-in `uniq` method!

class Array
  def my_uniq
    uniq = []

    count = 0
    while count < self.length
      uniq.push(self[count]) if !uniq.include? self[count]
      count += 1
    end

    uniq
  end
end

# Two Sum
#
# Write a new `Array#two_sum` method that finds all pairs of positions
# where the elements at those positions sum to zero.
#
# NB: ordering matters. I want each of the pairs to be sorted smaller
# index before bigger index. I want the array of pairs to be sorted
# "dictionary-wise":
#
# ```ruby
# [-1, 0, 2, -2, 1].two_sum # => [[0, 4], [2, 3]]
# ```
#
# * `[0, 2]` before `[1, 2]` (smaller first elements come first)
# * `[0, 1]` before `[0, 2]` (then smaller second elements come first)

class Array
  def two_sum
    return if self.length < 2
    pairs = []

    out_count = 0
    while out_count < self.length - 1
      in_count = out_count + 1
      while in_count < self.length
        if self[out_count] + self[in_count] == 0
          pairs.push([out_count, in_count])
        end
        in_count += 1
      end
      out_count += 1
    end

    pairs
  end
end

# Median
#
# Write a method that finds the median of a given array of integers. If
# the array has an odd number of integers, return the middle item from the
# sorted array. If the array has an even number of integers, return the
# average of the middle two items from the sorted array.

class Array
  def median
    return nil if self.empty?

    sorted = self.sort
    if sorted.length % 2 == 0
      return two_num_mean(sorted[length / 2], sorted[length / 2 - 1])
    else
      return sorted[length / 2]
    end
  end

  def two_num_mean(num1, num2)
    ((num1.to_f + num2.to_f) / 2)
  end
end



# My Transpose
#
# To represent a *matrix*, or two-dimensional grid of numbers, we can
# write an array containing arrays which represent rows:
#
# ```ruby
# rows = [
#     [0, 1, 2],
#     [3, 4, 5],
#     [6, 7, 8]
#   ]
#
# row1 = rows[0]
# row2 = rows[1]
# row3 = rows[2]
# ```
#
# We could equivalently have stored the matrix as an array of
# columns:
#
# ```ruby
# cols = [
#     [0, 3, 6],
#     [1, 4, 7],
#     [2, 5, 8]
#   ]
# ```
#
# Write a method, `my_transpose`, which will convert bettuween the
# row-oriented and column-oriented representations. You may assume square
# matrices for simplicity's sake. Usage will look like the following:
#
# ```ruby
# matrix = [
#   [0, 1, 2],
#   [3, 4, 5],
#   [6, 7, 8]
# ]
#
# matrix.my_transpose
#  # => [[0, 3, 6],
#  #    [1, 4, 7],
#  #    [2, 5, 8]]
# ```
#
# Don't use the built-in `transpose` method!

class Array
  def my_transpose
    transpose = []
    self[0].length.times { transpose.push([]) }

    dimension1 = 0
    while dimension1 < self[0].length
      dimension2 = 0
      while dimension2 < self.length
        transpose[dimension1].push(self[dimension2][dimension1])
        dimension2 += 1
      end
      dimension1 += 1
    end

    transpose
  end
end

# Bonus: Refactor your `Array#my_transpose` method to work with any rectangular
# matrix (not necessarily a square one).
